export default {
    url: {
        basicUrl:
            process.env.NODE_ENV === 'development'
                ? 'http://10.25.26.193:8080/prediction/'
                : 'http://10.25.26.193:8080/prediction/',
    },
};