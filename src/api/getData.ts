import { _get, _post, _delete } from './index';

// 登录
export const findPageUser = (data: object) => {
    const req = {
        data,
        url: 'user/findPageUser',
    };
    return _get(req);
};

// 用户详情
export const findOneUser = (data: object) => {
    const req = {
        data,
        url: 'user/findOneUser',
    };
    return _get(req);
};

// 商品详情
export const findUserOrderPredict = (data: object) => {
    const req = {
        data,
        url: 'user/findUserOrderPredict',
    };
    return _get(req);
};

//商品
export const findPageItem = (data: object) => {
    const req = {
        data,
        url: 'item/findPageItem',
    };
    return _get(req);
};

//AI理财接口
export const findPageHotItem = (data: object) => {
    const req = {
        data,
        url: 'item/findPageHotItem',
    };
    return _get(req);
};

export const findPageHotItemDetail = (data: object) => {
    const req = {
        data,
        url: 'item/findPageHotItemDetail',
    };
    return _get(req);
};


//AI理财用户展示接口
export const findItemUserDetail = (data: object) => {
    const req = {
        data,
        url: 'item/findItemUserDetail',
    };
    return _get(req);
};