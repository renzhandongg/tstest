import Vue from 'vue'
import Router from 'vue-router'
import iView from 'iview'

Vue.use(Router)

const router = new Router({
    routes: [{
        path: '/',
        name: 'index',
        component: () => import('../views/index.vue'),
        children: [{
            path: '/user',
            name: 'user',
            component: () => import('../views/user/index.vue'),
        },
        {
            path: '/commodity',
            name: 'commodity',
            component: () => import('../views/commodity/index.vue'),
        },
        {
            path: '/economic',
            name: 'economic',
            component: () => import('../views/economic/index.vue'),
        },
        {
            path: '/AI',
            name: 'AI',
            component: () => import('../views/AI/index.vue'),
        },
        {
            path: '/matchUser',
            name: 'matchUser',
            component: () => import('../views/AI/matchUser.vue'),
        },
        ]
    },]
})
router.beforeEach((to, from, next) => {
    (iView as any).LoadingBar.start();
    if (to.path === '/') {
        next('/user')
    } else {
        next()
    }
})
router.afterEach(() => {
    (iView as any).LoadingBar.finish()
})
export default router;
